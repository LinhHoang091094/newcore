﻿using Core.Helpers;
using Microsoft.Extensions.Configuration;
using MyServices.Repository;
using MyServices.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyServices
{
    public class UnitOfWork
    {
        
        public IConfiguration Configuration;

        public UnitOfWork(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }
        public RoleRepository roleRepository;
        public RoleRepository RoleRepository
        {
            get
            {
                if (!Utils.NotIsNullOrEmpty(this.roleRepository))
                {
                    this.roleRepository = new RoleRepository("Role", Configuration);
                }
                return this.roleRepository;
            }
        }
        public UserRepository userRepository;
        public UserRepository UserRepository
        {
            get
            {
                if (!Utils.NotIsNullOrEmpty(this.userRepository))
                {
                    this.userRepository = new UserRepository("Users", Configuration);
                }
                return this.userRepository;
            }
        }
        public UserRoleRepository userRoleRepository;
        public UserRoleRepository UserRoleRepository
        {
            get
            {
                if (!Utils.NotIsNullOrEmpty(this.userRoleRepository))
                {
                    this.userRoleRepository = new UserRoleRepository("UserRole", Configuration);
                }
                return this.userRoleRepository;
            }
        }
        public OrderRepository orderRepository { get; set; }
        public OrderRepository OrderRepository
        {
            get
            {
                if (!Utils.NotIsNullOrEmpty(this.orderRepository))
                {
                    this.orderRepository = new OrderRepository("Order", Configuration);
                }
                return this.orderRepository;
            }
        }
        public ProductRepository productRepository { get; set; }
        public ProductRepository ProductRepository
        {
            get
            {
                if (!Utils.NotIsNullOrEmpty(this.productRepository))
                {
                    this.productRepository = new ProductRepository("Product", Configuration);
                }
                return this.productRepository;
            }
        }
        public CategoryRepository categoryRepository { get; set; }
        public CategoryRepository CategoryRepository
        {
            get
            {
                if (!Utils.NotIsNullOrEmpty(this.categoryRepository))
                {
                    this.categoryRepository = new CategoryRepository("Category", Configuration);
                }
                return this.categoryRepository;
            }
        }
        public EventRepository eventRepository { get; set; }
        public EventRepository EventRepository
        {
            get
            {
                if (!Utils.NotIsNullOrEmpty(this.eventRepository))
                {
                    this.eventRepository = new EventRepository("Event", Configuration);
                }
                return this.eventRepository;
            }
        }
    }
}
