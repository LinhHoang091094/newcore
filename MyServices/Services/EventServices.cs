﻿using Core.Helpers;
using Core.Models.Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Services
{
    public class EventServices
    {
        private UnitOfWork _unitOfWork;
        public IConfiguration _configuration;
        public EventServices(UnitOfWork unitOfWork, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Event>> GetAll()
        {
            try
            {
                //db
                var lst = await _unitOfWork.EventRepository.GetAllAsync();
                return lst;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<Event> GetByID(int id)
        {
            try
            {
                //db
                var detail = await _unitOfWork.EventRepository.GetByIDAsync(id);
                return detail;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<int> Create(Event events, Users currentUser)
        {
            var insert = 0;
            try
            {
                CreateEvent(ref events, currentUser);
                //db
                insert = await _unitOfWork.EventRepository.InsertAsync(events);
                return insert;
            }
            catch (Exception e)
            {

            }
            return insert;
        }

        public async Task<int> Update(Event events, Users currentUser)
        {
            var update = 0;
            try
            {
                UpdateEvent(ref events, currentUser);
                //db
                update = await _unitOfWork.EventRepository.UpdateAsync(events);
                return update;
            }
            catch (Exception e)
            {

            }
            return update;
        }
        public async Task Delete(int id)
        {
            try
            {
                await _unitOfWork.EventRepository.DeleteAsync(id);
            }
            catch (Exception e)
            {

            }
        }
        #region 
        public void CreateEvent(ref Event events, Users currentUser)
        {
            events.Created = DateTime.Now;
            events.CreatedBy = currentUser.IDUser;
        }


        public void UpdateEvent(ref Event events, Users currentUser)
        {
            events.Updated = DateTime.Now;
            events.UpdatedBy = currentUser.IDUser;
        }

        #endregion
    }
}
