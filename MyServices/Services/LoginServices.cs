﻿using Core.Helpers;
using Core.Models.APIModels;
using Core.Models.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Services
{
    public class LoginServices
    {
        private UnitOfWork _unitOfWork;
        private readonly AppSettings _appSettings;
        public IConfiguration _configuration;
        public LoginServices(UnitOfWork unitOfWork, IConfiguration Configuration, IOptions<AppSettings> appSettings)
        {
            _configuration = Configuration;
            _unitOfWork = unitOfWork;
            _appSettings = appSettings.Value;
        }
        public bool Validate(ref Users user, ref string error)
        {
            try
            {
                //db
                user = _unitOfWork.UserRepository.Validate(user.UserName, Utils.GetMD5(user.Password));

                //db
                if (Utils.NotIsNullOrEmpty(user) && user.IDUser > 0)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                error = e.Message;
            }

            return false;
        }

        public async Task<Users> GetById(int userId)
        {
            return await _unitOfWork.UserRepository.GetByIDAsync(userId);
        }

        public Users GetByIdIdentity(string userId)
        {
            return _unitOfWork.UserRepository.GetByIdIdetityAsync(userId);
        }

        public AuthenticateResponse Authenticate(ref Users user, ref string error)
        {
            try
            {
                //db
                var oldUser = _unitOfWork.UserRepository.Validate(user.UserName, Utils.GetMD5(user.Password));

                //db
                if (Utils.NotIsNullOrEmpty(oldUser) && oldUser.IDUser > 0)
                {
                    var token = generateJwtToken(user);
                    oldUser.Id = user.Id;
                    var updated = _unitOfWork.UserRepository.Update(oldUser, new string[] { "Id" });
                    if (updated > 0)
                    {
                        user = oldUser;
                        return new AuthenticateResponse(user, token);
                    }
                    else
                        return null;
                }
            }
            catch (Exception e)
            {
                error = e.Message;
            }

            return null;
        }


        // helper methods
        private string generateJwtToken(Users user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_appSettings.Secret);//Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("Id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                //SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
