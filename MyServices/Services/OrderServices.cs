﻿using Core.Helpers;
using Core.Models.Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Services
{
    public class OrderServices
    {
        private UnitOfWork _unitOfWork;
        public IConfiguration _configuration;
        public OrderServices(UnitOfWork unitOfWork, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Create(Order product, Users currentUser)
        {
            return true;
        }

        public async Task<IEnumerable<Users>> GetAll()
        {
            try
            {
                //db
                var lst = await _unitOfWork.UserRepository.GetAllAsync();
                return lst;
            }
            catch (Exception e)
            {
                
            }

            return null;
        }
    }
}
