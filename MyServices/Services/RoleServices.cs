﻿using Core.Helpers;
using Core.Models.Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Services
{
    public class RoleServices
    {
        private UnitOfWork _unitOfWork;
        public IConfiguration _configuration;
        public RoleServices(UnitOfWork unitOfWork, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Role>> GetAll()
        {
            try
            {
                //db
                var lst = await _unitOfWork.RoleRepository.GetAllAsync();
                return lst;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<Role> GetByID(int id)
        {
            try
            {
                //db
                var detail = await _unitOfWork.RoleRepository.GetByIDAsync(id);
                return detail;
            }
            catch (Exception e)
            {

            }

            return null;
        }
        public async Task<IEnumerable<Role>> GetRoleByUser(int userID)
        {
            try
            {
                //db
                var lst = await _unitOfWork.UserRoleRepository.GetRoleByUser(userID);
                return lst;
            }
            catch (Exception e)
            {

            }

            return null;
        }
    }
}
