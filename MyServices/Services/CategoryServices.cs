﻿using Core.Helpers;
using Core.Models.Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Services
{
    public class CategoryServices
    {
        private UnitOfWork _unitOfWork;
        public IConfiguration _configuration;
        public CategoryServices(UnitOfWork unitOfWork, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            try
            {
                //db
                var lst = await _unitOfWork.CategoryRepository.GetAllAsync();
                return lst;
            }
            catch (Exception e)
            {

            }

            return null;
        }
        public async Task<Dictionary<Category,List<Category>>> GetListDic()
        {
            try
            {
                var rs= new Dictionary<Category, List<Category>>();
                //db
                var lst = await _unitOfWork.CategoryRepository.GetAllAsync();
                var cateParent = lst.ToList().Where(x=>x.IDParent==0);
                return rs;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<Category> GetByID(int id)
        {
            try
            {
                //db
                var detail = await _unitOfWork.CategoryRepository.GetByIDAsync(id);
                return detail;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<int> Create(Category category, Users currentUser)
        {
            var insert = 0;
            try
            {
                CreateCategory(ref category, currentUser);
                //db
                insert = await _unitOfWork.CategoryRepository.InsertAsync(category);
                return insert;
            }
            catch (Exception e)
            {

            }
            return insert;
        }

        public async Task<int> Update(Category category, Users currentUser)
        {
            var update = 0;
            try
            {
                UpdateCategory(ref category, currentUser);
                //db
                update = await _unitOfWork.CategoryRepository.UpdateAsync(category);
                return update;
            }
            catch (Exception e)
            {

            }
            return update;
        }
        public async Task Delete(int id)
        {
            try
            {
                await _unitOfWork.CategoryRepository.DeleteAsync(id);
            }
            catch (Exception e)
            {

            }
        }
        #region 
        public void CreateCategory(ref Category category, Users currentUser)
        {
            category.Created = DateTime.Now;
            category.CreatedBy = currentUser.IDUser;
        }


        public void UpdateCategory(ref Category category, Users currentUser)
        {
            category.Updated = DateTime.Now;
            category.UpdatedBy = currentUser.IDUser;
        }

        #endregion
    }
}
