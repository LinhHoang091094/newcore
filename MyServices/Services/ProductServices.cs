﻿using Core.Helpers;
using Core.Models.Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Services
{
    public class ProductServices
    {
        private UnitOfWork _unitOfWork;
        public IConfiguration _configuration;
        public ProductServices(UnitOfWork unitOfWork, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            try
            {
                //db
                var lst = await _unitOfWork.ProductRepository.GetAllAsync();
                return lst;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<Product> GetByID(int id)
        {
            try
            {
                //db
                var detail = await _unitOfWork.ProductRepository.GetByIDAsync(id);
                return detail;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public async Task<int> Create(Product product, Users currentUser)
        {
            var insert = 0;
            try
            {
                CreateProduct(ref product, currentUser);
                //db
                insert = await _unitOfWork.ProductRepository.InsertAsync(product);
                return insert;
            }
            catch (Exception e)
            {

            }
            return insert;
        }

        public async Task<int> Update(Product product, Users currentUser)
        {
            var update = 0;
            try
            {
                UpdateProduct(ref product, currentUser);
                //db
                update = await _unitOfWork.ProductRepository.UpdateAsync(product);
                return update;
            }
            catch (Exception e)
            {

            }
            return update;
        }
        public async Task Delete(int id)
        {
            try
            {
                await _unitOfWork.ProductRepository.DeleteAsync(id);
            }
            catch (Exception e)
            {

            }
        }
        #region 
        public void CreateProduct(ref Product product, Users currentUser)
        {
            product.Created = DateTime.Now;
            product.CreatedBy = currentUser.IDUser;
        }


        public void UpdateProduct(ref Product product, Users currentUser)
        {
            product.Updated = DateTime.Now;
            product.UpdatedBy = currentUser.IDUser;
        }

        #endregion
    }
}
