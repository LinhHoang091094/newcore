﻿using Core.Helpers;
using Core.Models.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Repository
{
    public abstract class GenericRepository<T>
    {
        #region general
        protected readonly string _tableName;
        private MySqlConnection conn;
        public IConfiguration _configuration;
        public GenericRepository(string tableName, IConfiguration Configuration)
        {
            _tableName = tableName;
            _configuration = Configuration;
        }
        /// <summary>
        /// Generate new connection based on connection string
        /// </summary>
        /// <returns></returns>
        private MySqlConnection MySqlConnection()
        {
            //var str = ConfigurationExtensions
            //       .GetConnectionString(this._configuration, "DefaultConnection");
            var str = ConfigurationExtensions
                   .GetConnectionString(this._configuration, "MariaDbConnectionString");
            return new MySqlConnection(str);
        }
        /// <summary>
        /// Open new connection and return it for use
        /// </summary>
        /// <returns></returns>
        protected IDbConnection CreateConnection()
        {
            if (conn == null || !Utils.NotIsNullOrEmpty(conn.ConnectionString))
            {
                if (conn == null)
                {
                    conn = MySqlConnection();
                    conn.Open();
                }
            }
            if (conn.State == ConnectionState.Closed)
            {
                conn.Dispose();
                conn = MySqlConnection();
                conn.Open();
            }

            return conn;
        }
        #endregion
        #region get
        public IEnumerable<T> GetAll()
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    return connection.Query<T>($"SELECT * FROM {_tableName}");
                }
                catch (Exception)
                {
                    connection.Close();
                    connection.Dispose();

                }
            }
            return Enumerable.Empty<T>();
        }
        public int GetCount()
        {
            using (var connection = CreateConnection())
            {
                return connection.ExecuteScalar<int>($"SELECT COUNT(ID) FROM {_tableName}");

            }

        }
        public T GetByIdIdetityAsync(string id)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    var result = connection.QuerySingleOrDefault<T>($"SELECT * FROM {_tableName} WHERE Id=@Id", new { Id = id });
                    return result;
                }
                catch (Exception)
                {
                    return default;
                }
            }
        }
        public async Task<T> GetByIDAsync(int id)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    var result = await connection.QuerySingleOrDefaultAsync<T>($"SELECT * FROM {_tableName} WHERE ID=@Id", new { Id = id });
                    return result;
                }
                catch (Exception)
                {
                    return default;
                }
            }
        }
        public T GetByID(int id)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    var result = connection.QuerySingleOrDefault<T>($"SELECT * FROM {_tableName} WHERE ID=@Id", new { Id = id });
                    return result;
                }
                catch (Exception)
                {
                    return default;
                }
            }
        }

        public IEnumerable<T> GetByIDs(int[] ids)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    var query = $"SELECT * FROM {_tableName} WHERE ID IN @Id";
                    var result = connection.Query<T>(query, new { Id = ids });
                    if (result == null)
                    {
                        return new List<T>();
                    }
                    return result;
                }
                catch (Exception)
                {
                    return new List<T>();
                }
            }
        }

        public async Task<IEnumerable<T>> GetByIDsAsync(int[] ids)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    var query = $"SELECT * FROM {_tableName} WHERE ID IN @Id";
                    var result = await connection.QueryAsync<T>(query, new { Id = ids });
                    if (result == null)
                    {
                        return new List<T>();
                    }
                    return result;
                }
                catch (Exception)
                {
                    return new List<T>();
                }
            }
        }

        #endregion
        public void Delete(int id)
        {
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    connection.Execute($"DELETE FROM {_tableName} WHERE ID=@Id", new { Id = id }, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
        }
        public void DeleteWithTrans(int id, IDbTransaction transaction)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    connection.Execute($"DELETE FROM {_tableName} WHERE ID=@Id", new { Id = id }, transaction);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }
        public async Task DeleteAsync(int id)
        {
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    await connection.ExecuteAsync($"DELETE FROM {_tableName} WHERE ID=@Id", new { Id = id }, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
        }
        public async Task DeleteWithTransAsync(int id, IDbTransaction transaction)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    await connection.ExecuteAsync($"DELETE FROM {_tableName} WHERE ID=@Id", new { Id = id }, transaction);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }

            }
        }
        public void DeleteRows(int[] ids)
        {
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    connection.Execute($"DELETE FROM {_tableName} WHERE ID in @Id", new { Id = ids }, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
        }
        public void DeleteRowsWithTrans(int[] ids, IDbTransaction transaction)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    connection.Execute($"DELETE FROM {_tableName} WHERE ID in @Id", new { Id = ids }, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }
        public async Task DeleteRowsAsync(int[] ids)
        {
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    await connection.ExecuteAsync($"DELETE FROM {_tableName} WHERE ID in @Id", new { Id = ids }, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
        }
        public async Task DeleteRowsAsyncWithTrans(int[] ids, IDbTransaction transaction)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    await connection.ExecuteAsync($"DELETE FROM {_tableName} WHERE ID in @Id", new { Id = ids }, transaction);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }
        #region update
        public int Update(T t, string[] arr = null)
        {
            var updated = 0;
            var updateQuery = GenerateUpdateQuery(arr);
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    updated = connection.Execute(updateQuery, t, transaction);
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
            return updated;
        }
        public int UpdateWithTrans(T t, IDbTransaction transaction, string[] arr = null)
        {
            var updated = 0;
            var updateQuery = GenerateUpdateQuery(arr);
            using (var connection = CreateConnection())
            {
                try
                {
                    updated = connection.Execute(updateQuery, t, transaction);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
            }
            return updated;
        }
        public async Task<int> UpdateAsync(T t, string[] arr = null)
        {
            var updated = 0;
            var updateQuery = GenerateUpdateQuery(arr);
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    updated = await connection.ExecuteAsync(updateQuery, t, transaction);
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
            return updated;
        }
        public async Task<int> UpdateAsyncWithTrans(T t, IDbTransaction transaction, string[] arr = null)
        {
            var updated = 0;
            var updateQuery = GenerateUpdateQuery(arr);
            using (var connection = CreateConnection())
            {
                try
                {
                    updated = await connection.ExecuteAsync(updateQuery, t, transaction);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
            }
            return updated;
        }
        #endregion

        #region insert
        public int Insert(T t)
        {
            var inserted = 0;
            var insertQuery = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    inserted = connection.Execute(insertQuery, t, transaction);
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
            return inserted;
        }
        public int InsertWithTrans(T t, IDbTransaction transaction)
        {
            var inserted = 0;
            var insertQuery = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                try
                {
                    inserted = connection.Execute(insertQuery, t, transaction);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
            }
            return inserted;
        }
        public async Task<int> InsertAsync(T t)
        {
            var inserted = 0;
            var insertQuery = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    inserted = await connection.ExecuteAsync(insertQuery, t, transaction);
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
            return inserted;
        }
        public async Task<int> InsertAsyncWithTrans(T t, IDbTransaction transaction)
        {
            var inserted = 0;
            var insertQuery = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                try
                {
                    inserted = await connection.ExecuteAsync(insertQuery, t, transaction);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
            }
            return inserted;
        }
        public string GenerateInsertQueryInsertReturn()
        {

            var insertQuery = new StringBuilder($" INSERT INTO {_tableName} ");

            insertQuery.Append("(");
            var properties = GenerateListOfProperties(GetProperties);
            properties.ForEach(prop =>
            {
                if (prop != "ID")
                {
                    insertQuery.Append($"`{prop}`,");
                }
            });
            insertQuery
                .Remove(insertQuery.Length - 1, 1)
                .Append(") VALUES (");
            properties.ForEach(prop =>
            {
                if (prop != "ID")
                {
                    insertQuery.Append($"@{prop},");
                }
            });
            insertQuery
                .Remove(insertQuery.Length - 1, 1)
                .Append("); ");
            var strOutput = $" select * from {_tableName} where Id=(SELECT LAST_INSERT_ID());";
            insertQuery.Append(strOutput);
            return insertQuery.ToString();

        }

        public int SaveRange(IEnumerable<T> list)
        {
            var inserted = 0;
            var query = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    inserted += connection.Execute(query, list, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

            }
            return inserted;
        }
        public int SaveRangeWithTrans(IEnumerable<T> list, IDbTransaction transaction)
        {
            var inserted = 0;
            var query = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                try
                {
                    inserted += connection.Execute(query, list, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

            }
            return inserted;
        }
        public async Task<int> SaveRangeAsync(IEnumerable<T> list)
        {
            var inserted = 0;
            var query = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    inserted += await connection.ExecuteAsync(query, list, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

            }
            return inserted;
        }
        public async Task<int> SaveRangeAsyncWithTrans(IEnumerable<T> list, IDbTransaction transaction)
        {
            var inserted = 0;
            var query = GenerateInsertQuery();
            using (var connection = CreateConnection())
            {
                try
                {
                    inserted += await connection.ExecuteAsync(query, list, transaction);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            return inserted;
        }

        public IEnumerable<T> SaveRangeReturn(IEnumerable<T> list)
        {
            var query = GenerateInsertQuery();
            //lay id
            query += $" select top " + list.Count() + " * from {_tableName} order by id desc ";
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    list = connection.Query<T>(query, list, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

            }
            return list;
        }
        public IEnumerable<T> SaveRangeReturnWithTrans(IEnumerable<T> list, IDbTransaction transaction)
        {
            var query = GenerateInsertQuery();
            //lay id
            query += $" select top " + list.Count() + " * from {_tableName} order by id desc ";
            using (var connection = CreateConnection())
            {
                try
                {
                    list = connection.Query<T>(query, list, transaction);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

            }
            return list;
        }
        public async Task<IEnumerable<T>> SaveRangeReturnAsync(IEnumerable<T> list)
        {
            var query = GenerateInsertQuery();
            //lay id
            query += $" select top " + list.Count() + " * from {_tableName} order by id desc ";
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    list = await connection.QueryAsync<T>(query, list, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

            }
            return list;
        }
        public async Task<IEnumerable<T>> SaveRangeReturnAsyncWithTrans(IEnumerable<T> list, IDbTransaction transaction)
        {
            var query = GenerateInsertQuery();
            //lay id
            query += $" select top " + list.Count() + " * from {_tableName} order by id desc ";
            using (var connection = CreateConnection())
            {
                try
                {
                    list = await connection.QueryAsync<T>(query, list, transaction);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            return list;
        }


        #endregion
        #region gen query
        protected string GenerateUpdateQuery(string[] arr = null)
        {
            var updateQuery = new StringBuilder($"UPDATE {_tableName} SET ");
            if (arr == null)
            {
                var properties = GenerateListOfProperties(GetProperties);
                properties.ForEach(property =>
                {
                    if (!property.Equals("ID"))
                    {
                        //updateQuery.Append($"`{property}`=@{property},");
                        updateQuery.Append($"`{property}`=@{property},");
                    }
                });
            }
            else
            {
                var properties = GenerateListOfProperties(GetProperties);
                foreach (var item in arr)
                {
                    updateQuery.Append($"`{item}`=@{item},");
                }
            }
            updateQuery.Remove(updateQuery.Length - 1, 1); //remove last comma
            if (typeof(T) == typeof(Users))
            {
                updateQuery.Append(" WHERE IDUser = @IDUser");
            }
            else
            {
                updateQuery.Append(" WHERE ID = @ID");
            }
            return updateQuery.ToString();

        }
        protected string GenerateInsertQuery()
        {
            var insertQuery = new StringBuilder($" INSERT INTO {_tableName} ");

            insertQuery.Append("(");
            var properties = GenerateListOfProperties(GetProperties);
            properties.ForEach(prop =>
            {
                if (prop != "ID")
                {
                    insertQuery.Append($"`{prop}`,");
                }
            });
            insertQuery
                .Remove(insertQuery.Length - 1, 1)
                .Append(") VALUES (");
            properties.ForEach(prop =>
            {
                if (prop != "ID")
                {
                    insertQuery.Append($"@{prop},");
                }
            });
            insertQuery
                .Remove(insertQuery.Length - 1, 1)
                .Append(")");
            return insertQuery.ToString();
        }

        protected IEnumerable<PropertyInfo> GetProperties => typeof(T).GetProperties();
        protected static List<string> GenerateListOfProperties(IEnumerable<PropertyInfo> listOfProperties)
        {
            return (from prop in listOfProperties
                    let attributes = prop.GetCustomAttributes(typeof(DescriptionAttribute), false)
                    where attributes.Length <= 0 || (attributes[0] as DescriptionAttribute)?.Description != "ignore"
                    select prop.Name).ToList();
        }
        #endregion
        #region extend method
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    return await connection.QueryAsync<T>($"SELECT * FROM {_tableName}");
                }
                catch (Exception)
                {
                    connection.Close();
                    connection.Dispose();

                }
            }
            return Enumerable.Empty<T>();
        }


        public async Task<T> GetAsync(int id)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    var result = await connection.QuerySingleOrDefaultAsync<T>($"SELECT * FROM {_tableName} WHERE ID=@Id", new { Id = id });
                    return result;
                }
                catch (Exception)
                {
                    return default;
                }
            }
        }


        public async Task<IEnumerable<T>> CusExecuteQuery(string sql)
        {
            using (var connection = CreateConnection())
            {
                var transaction = connection.BeginTransaction();
                try
                {
                    var result = await connection.QueryAsync<T>(sql, transaction);
                    transaction.Commit();
                    return result;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }
            }
            return Enumerable.Empty<T>();
        }
        #endregion
    }
}
