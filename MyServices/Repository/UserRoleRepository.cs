﻿using Core.Models.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using MyServices.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Repository
{
    public class UserRoleRepository : GenericRepository<UserRole>
    {
        public UserRoleRepository(string tableName, IConfiguration Configuration) : base(tableName, Configuration)
        {

        }

        public async Task<IEnumerable<Role>> GetRoleByUser(int userID)
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    var result = await connection.QueryAsync<Role>($"SELECT * FROM userrole ur left join role r on ur.IDRole = r.IDRole where ur.IDUser = @Id", new { Id = userID });
                    return result;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
