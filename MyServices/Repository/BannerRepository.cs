﻿using Core.Models.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using MyServices.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyServices.Repository
{
    public class BannerRepository : GenericRepository<Banner>
    {
        public BannerRepository(string tableName, IConfiguration Configuration) : base(tableName, Configuration)
        {
           
        }
    }
}
