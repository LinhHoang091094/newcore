﻿using Core.Models.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using MyServices.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Repository
{
    public class RoleRepository : GenericRepository<Role>
    {
        public RoleRepository(string tableName, IConfiguration Configuration) : base(tableName, Configuration)
        {
           
        }

    }
}
