﻿using Core.Models.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using MyServices.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyServices.Repository
{
    public class UserRepository : GenericRepository<Users>
    {
        public UserRepository(string tableName, IConfiguration Configuration) : base(tableName, Configuration)
        {
           
        }
        public Users Validate(string username, string pass)
        {
            using (var connection = CreateConnection())
            {
                var result = connection.QueryFirstOrDefault<Users>($"SELECT * FROM {_tableName} WHERE Username=@Username AND Password=@Password", new { Username = username, Password = pass });
                if (result == null)
                    throw new KeyNotFoundException($"Tài khoản hoặc mật khẩu không đúng");
                return result;
            }
        }
    }
}
