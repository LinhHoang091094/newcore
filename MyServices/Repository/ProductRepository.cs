﻿using Core.Models.Entity;
using Dapper;
using Microsoft.Extensions.Configuration;
using MyServices.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyServices.Repository
{
    public class ProductRepository : GenericRepository<Product>
    {
        public ProductRepository(string tableName, IConfiguration Configuration) : base(tableName, Configuration)
        {
           
        }
    }
}
