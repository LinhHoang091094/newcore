﻿using Core.Helpers;
using Core.Models.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MyServices.Extension;
using MyServices.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyServices
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //inject identity 
            services.AddIdentity<Users, Role>()
               .AddDefaultTokenProviders();

            services.AddScoped<IUserStore<Users>, ExtensionUserStore>();
            services.AddScoped<IRoleStore<Role>, ExtensionRoleStore>();

            //register service
            services.AddScoped<UnitOfWork>();

            services.AddScoped<LoginServices>();
            services.AddScoped<ProductServices>();
            services.AddScoped<OrderServices>();
            services.AddScoped<RoleServices>();
        }
    }
}
