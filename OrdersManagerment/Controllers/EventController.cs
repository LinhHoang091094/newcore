﻿using Core.Helpers;
using Core.Models.APIModels;
using Core.Models.Entity;
using Core.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MyServices.Services;
using Newtonsoft.Json;
using OrdersManagerment.Auth.Author;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrdersManagerment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EventController : Controller
    {
        private readonly EventServices _eventService;
        public IConfiguration _configuration;
        public Users CurrentUser;

        public UserManager<Users> _userManager { get; }
        public SignInManager<Users> _signInManager { get; }
        public EventController(EventServices eventService, IConfiguration Configuration, UserManager<Users> UserManager, SignInManager<Users> SignInManager)
        {
            _configuration = Configuration;
            _eventService = eventService;
            _userManager = UserManager;
            _signInManager = SignInManager;
        }
        #region list
        [HttpGet("Index")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("getlist")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> GetList()
        {
            var lst = await _eventService.GetAll();
            return Json(new BaseResponse()
            {
                StatusCode = 0,
                Message = "OK",
                Result = lst
            });
        }
        #endregion
        #region detail
        //public IActionResult Details(int id)
        //{
        //    return View();
        //}
        [HttpPost("Details")]
        public async Task<IActionResult> Details(int id)
        {
            var detail = await _eventService.GetByID(id);
            return Json(new BaseResponse()
            {
                StatusCode = 0,
                Message = "OK",
                Result = detail
            });
        }
        #endregion

        #region create
        [HttpGet]
        public IActionResult Create()
        {
            var model = new EventViewModel();
            model.Events  = new List<Event>();
            return PartialView("Create", model);
        }
        [HttpPost("Creat")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Creat([FromBody] Event events)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isCreate = await _eventService.Create(events, CurrentUser);
                    if (isCreate > 0)
                    {
                        if (Request.IsAjaxRequest())
                        {
                            return Json(new { success = true, message = "Success", redirectUrl = Url.Action("Index", "Event"), isNotList = false });
                        }
                        else
                        {
                            return RedirectToAction("index");
                        }

                    }
                    else
                    {
                        return Json(new { success = false, message = "Error" });
                    }
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = e.Message });
                }

            }
            return Json(new { success = false, message = "Error---invalid" });
        }


        //api
        [HttpPost("CreatEvent")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> CreatEvent([FromBody] Event events)
{

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isCreate = await _eventService.Create(events, CurrentUser);
                    if (isCreate > 0)
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = 0,
                            Message = "OK",
                            Result = null
                        });

                    }
                    else
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = -1,
                            Message = "Error",
                            Result = null
                        });
                    }
                }
                catch (Exception e)
                {
                    return Json(new BaseResponse()
                    {
                        StatusCode = -1,
                        Message = e.Message,
                        Result = null
                    });
                }

            }
            return Json(new BaseResponse()
            {
                StatusCode = -1,
                Message = "Error---invalid",
                Result = null
            });
        }
        #endregion
        #region update
        [HttpGet("Edit")]
        public IActionResult Edit(int id)
        {
            return View();
        }


        [HttpPost("UpdateEvent")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> UpdateEvent([FromBody] Event events)
{

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isUpdate = await _eventService.Update(events, CurrentUser);
                    if (isUpdate > 0)
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = 0,
                            Message = "OK",
                            Result = null
                        });

                    }
                    else
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = -1,
                            Message = "Error",
                            Result = null
                        });
                    }
                }
                catch (Exception e)
                {
                    return Json(new BaseResponse()
                    {
                        StatusCode = -1,
                        Message = e.Message,
                        Result = null
                    });
                }

            }
            return Json(new BaseResponse()
            {
                StatusCode = -1,
                Message = "Error---invalid",
                Result = null
            });
        }
        #endregion
        #region delete
        [HttpPost("Delete")]
        public IActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost("DeleteEvent")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> DeleteEvent(int id = 0)
        {
            try
            {
                CurrentUser = (Users)HttpContext.Items["User"];
                await _eventService.Delete(id);
                return Json(new BaseResponse()
                {
                    StatusCode = 0,
                    Message = "OK",
                    Result = null
                });

            }
            catch (Exception e)
            {
                return Json(new BaseResponse()
                {
                    StatusCode = -1,
                    Message = e.Message,
                    Result = null
                });
            }

        }
        #endregion
    }
}
