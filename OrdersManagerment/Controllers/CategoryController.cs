﻿using Core.Helpers;
using Core.Models.APIModels;
using Core.Models.Entity;
using Core.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MyServices.Services;
using Newtonsoft.Json;
using OrdersManagerment.Auth.Author;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrdersManagerment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly CategoryServices _categoryService;
        public IConfiguration _configuration;
        public Users CurrentUser;

        public UserManager<Users> _userManager { get; }
        public SignInManager<Users> _signInManager { get; }
        public CategoryController(CategoryServices categoryService, IConfiguration Configuration, UserManager<Users> UserManager, SignInManager<Users> SignInManager)
        {
            _configuration = Configuration;
            _categoryService = categoryService;
            _userManager = UserManager;
            _signInManager = SignInManager;
        }
        #region list
        [HttpGet("Index")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("getlist")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> GetList()
        {
            var lst = await _categoryService.GetAll();
            return Json(new BaseResponse()
            {
                StatusCode = 0,
                Message = "OK",
                Result = lst
            });
        }
        [HttpGet("GetListDic")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> GetListDic() //thu tu cha con
        {
            var lst = await _categoryService.GetListDic();
            return Json(new BaseResponse()
            {
                StatusCode = 0,
                Message = "OK",
                Result = lst
            });
        }
        #endregion
        #region detail
        //public IActionResult Details(int id)
        //{
        //    return View();
        //}
        [HttpPost("Details")]
        public async Task<IActionResult> Details(int id)
        {
            var detail = await _categoryService.GetByID(id);
            return Json(new BaseResponse()
            {
                StatusCode = 0,
                Message = "OK",
                Result = detail
            });
        }
        #endregion

        #region create
        [HttpGet]
        public IActionResult Create()
        {
            var model = new CategoryViewModel();
            model.Categories = new List<Category>();
            return PartialView("Create", model);
        }
        [HttpPost("Creat")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Creat([FromBody] Category category)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isCreate = await _categoryService.Create(category, CurrentUser);
                    if (isCreate > 0)
                    {
                        if (Request.IsAjaxRequest())
                        {
                            return Json(new { success = true, message = "Success", redirectUrl = Url.Action("Index", "Category"), isNotList = false });
                        }
                        else
                        {
                            return RedirectToAction("index");
                        }

                    }
                    else
                    {
                        return Json(new { success = false, message = "Error" });
                    }
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = e.Message });
                }

            }
            return Json(new { success = false, message = "Error---invalid" });
        }


        //api
        [HttpPost("CreatCategory")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> CreatCategory([FromBody] Category category)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isCreate = await _categoryService.Create(category, CurrentUser);
                    if (isCreate > 0)
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = 0,
                            Message = "OK",
                            Result = null
                        });

                    }
                    else
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = -1,
                            Message = "Error",
                            Result = null
                        });
                    }
                }
                catch (Exception e)
                {
                    return Json(new BaseResponse()
                    {
                        StatusCode = -1,
                        Message = e.Message,
                        Result = null
                    });
                }

            }
            return Json(new BaseResponse()
            {
                StatusCode = -1,
                Message = "Error---invalid",
                Result = null
            });
        }
        #endregion
        #region update
        [HttpGet("Edit")]
        public IActionResult Edit(int id)
        {
            return View();
        }


        [HttpPost("UpdateCategory")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> UpdateCategory([FromBody] Category category)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isUpdate = await _categoryService.Update(category, CurrentUser);
                    if (isUpdate > 0)
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = 0,
                            Message = "OK",
                            Result = null
                        });

                    }
                    else
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = -1,
                            Message = "Error",
                            Result = null
                        });
                    }
                }
                catch (Exception e)
                {
                    return Json(new BaseResponse()
                    {
                        StatusCode = -1,
                        Message = e.Message,
                        Result = null
                    });
                }

            }
            return Json(new BaseResponse()
            {
                StatusCode = -1,
                Message = "Error---invalid",
                Result = null
            });
        }
        #endregion
        #region delete
        [HttpPost("Delete")]
        public IActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost("DeleteCategory")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> DeleteCategory(int id = 0)
        {
            try
            {
                CurrentUser = (Users)HttpContext.Items["User"];
                await _categoryService.Delete(id);
                return Json(new BaseResponse()
                {
                    StatusCode = 0,
                    Message = "OK",
                    Result = null
                });

            }
            catch (Exception e)
            {
                return Json(new BaseResponse()
                {
                    StatusCode = -1,
                    Message = e.Message,
                    Result = null
                });
            }

        }
        #endregion
    }
}
