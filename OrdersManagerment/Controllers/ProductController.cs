﻿using Core.Helpers;
using Core.Models.APIModels;
using Core.Models.Entity;
using Core.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MyServices.Services;
using Newtonsoft.Json;
using OrdersManagerment.Auth.Author;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrdersManagerment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly ProductServices _productService;
        public IConfiguration _configuration;
        public Users CurrentUser;

        public UserManager<Users> _userManager { get; }
        public SignInManager<Users> _signInManager { get; }
        public ProductController(ProductServices productService, IConfiguration Configuration, UserManager<Users> UserManager, SignInManager<Users> SignInManager)
        {
            _configuration = Configuration;
            _productService = productService;
            _userManager = UserManager;
            _signInManager = SignInManager;
        }
        #region list
        [HttpGet("Index")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("GetList")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        //[Authorize]
        public async Task<IActionResult> GetList()
        {
            var lst = await _productService.GetAll();
            return Json(new BaseResponse()
            {
                StatusCode = 0,
                Message = "OK",
                Result = lst
            });
        }
        #endregion
        #region detail
        //public IActionResult Details(int id)
        //{
        //    return View();
        //}
        [HttpPost("Details")]
        public async Task<IActionResult> Details(int id)
        {
            var detail = await _productService.GetByID(id);
            return Json(new BaseResponse()
            {
                StatusCode = 0,
                Message = "OK",
                Result = detail
            });
        }
        #endregion

        #region create
        [HttpGet]
        public IActionResult Create()
        {
            var model = new ProductViewModel();
            model.Products = new List<Product>();
            return PartialView("Create", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Creat([FromForm] Product product)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isCreate = await _productService.Create(product, CurrentUser);
                    if (isCreate > 0)
                    {
                        if (Request.IsAjaxRequest())
                        {
                            return Json(new { success = true, message = "Success", redirectUrl = Url.Action("Index", "Products"), isNotList = false });
                        }
                        else
                        {
                            return RedirectToAction("index");
                        }

                    }
                    else
                    {
                        return Json(new { success = false, message = "Error" });
                    }
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = e.Message });
                }

            }
            return Json(new { success = false, message = "Error---invalid" });
        }


        //api
        [HttpPost("CreatProduct")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> CreatProduct([FromBody] Product product)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isCreate = await _productService.Create(product, CurrentUser);
                    if (isCreate > 0)
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = 0,
                            Message = "OK",
                            Result = null
                        });

                    }
                    else
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = -1,
                            Message = "Error",
                            Result = null
                        });
                    }
                }
                catch (Exception e)
                {
                    return Json(new BaseResponse()
                    {
                        StatusCode = -1,
                        Message = e.Message,
                        Result = null
                    });
                }

            }
            return Json(new BaseResponse()
            {
                StatusCode = -1,
                Message = "Error---invalid",
                Result = null
            });
        }
        #endregion
        #region update
        [HttpGet("Edit")]
        public IActionResult Edit(int id)
        {
            return View();
        }


        [HttpPost("UpdateProduct")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> UpdateProduct([FromBody] Product product)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    CurrentUser = (Users)HttpContext.Items["User"];
                    var isUpdate = await _productService.Update(product, CurrentUser);
                    if (isUpdate > 0)
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = 0,
                            Message = "OK",
                            Result = null
                        });

                    }
                    else
                    {
                        return Json(new BaseResponse()
                        {
                            StatusCode = -1,
                            Message = "Error",
                            Result = null
                        });
                    }
                }
                catch (Exception e)
                {
                    return Json(new BaseResponse()
                    {
                        StatusCode = -1,
                        Message = e.Message,
                        Result = null
                    });
                }

            }
            return Json(new BaseResponse()
            {
                StatusCode = -1,
                Message = "Error---invalid",
                Result = null
            });
        }
        #endregion
        #region delete
        [HttpGet("Delete")]
        public IActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost("DeleteProduct")]
        [CusAuthorizePermission(Permissions = "ADMIN")]
        public async Task<IActionResult> DeleteProduct(int id = 0)
        {
            try
            {
                CurrentUser = (Users)HttpContext.Items["User"];
                await _productService.Delete(id);
                return Json(new BaseResponse()
                {
                    StatusCode = 0,
                    Message = "OK",
                    Result = null
                });

            }
            catch (Exception e)
            {
                return Json(new BaseResponse()
                {
                    StatusCode = -1,
                    Message = e.Message,
                    Result = null
                });
            }

        }
        #endregion
    }
}
