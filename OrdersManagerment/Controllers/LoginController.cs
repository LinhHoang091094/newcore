﻿using Core.Helpers;
using Core.Models.APIModels;
using Core.Models.Entity;
using Core.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyServices;
using MyServices.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrdersManagerment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly LoginServices _loginService;
        public IConfiguration _configuration;

        public UserManager<Users> _userManager { get; }
        public SignInManager<Users> _signInManager { get; }
        // GET: LoginController
        public LoginController(ILogger<HomeController> logger, LoginServices loginService, IConfiguration Configuration, UserManager<Users> UserManager, SignInManager<Users> SignInManager)
        {
            _logger = logger;
            _configuration = Configuration;
            _loginService = loginService;
            _userManager = UserManager;
            _signInManager = SignInManager;
        }
        [HttpGet("Index")]
        public IActionResult Index()
        {
            LoginViewModel login = new LoginViewModel();
            login.Action = "Login/Authenticate";
            return View(login);
        }
        #region login
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromForm] Users user)
        {
            var error = "";
            var passNonMD5 = user.Password;
            var validateLogin = _loginService.Validate(ref user, ref error);
            if (validateLogin)
            {
                var result = await _userManager.FindByNameAsync(user.UserName);
                if (Utils.NotIsNullOrEmpty(result))
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User already in manager account with password.");
                    return RedirectToAction("Index", "ManageOrders");
                }
                else
                {
                    var result1 = await _userManager.CreateAsync(user, passNonMD5);
                    if (result1.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation(3, "User created a new account with password.");
                        return RedirectToAction("Index", "ManageOrders");
                    }
                }

            }
            return Json(new { success = false, message = error });
        }

        [HttpPost("Authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]AuthenticateRequest userDto)
        {
            if(!ModelState.IsValid) return Json(new { success = false, message = "Object validate error" });
            var error = "";
            var passNonMD5 = userDto.Password;
            Users user = new Users()
            {
                UserName = userDto.Username,
                Password = userDto.Password
            };
            var rs = _loginService.Authenticate(ref user, ref error);
            if (Utils.NotIsNullOrEmpty(rs))
            {
                var result = await _userManager.FindByNameAsync(user.UserName);
                if (Utils.NotIsNullOrEmpty(result))
                {
                    user.Name = user.UserName;
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User already in manager account with password.");
                    return Json(new { success = false, message = error });
                }
                else
                {
                    user.Name = user.UserName;
                    user.PasswordHash = user.Password;
                    var result1 = await _userManager.CreateAsync(user, passNonMD5);
                    if (result1.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation(3, "User created a new account with password.");
                        return Json(rs);
                    }
                }

            }
            return Json(new { success = false, message = error });
        }
        #endregion
        #region log out
        [HttpPost("LogOff")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
        #endregion
    }
}
