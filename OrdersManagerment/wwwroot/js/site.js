﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(document).ready(function () {
    $(document).ready(function () {

        $('.quickModal').click(function (e) {

            /*
            * hủy liên kết
            *
            * việc này quan trọng
            * nếu không hủy liên kết, modal sẽ không được load đúng cách
            * vì action sẽ trả về 1 trang html trống với partialview
            *
            * cách tương tự return false; cách này thực hiện ở cuối method
            */
            e.preventDefault();

            var $modal = $($(this).attr('data-target'));
            var $modalDialog = $('.modal-dialog');
            var href = $(this).attr('data-url');

            // không cho phép tắt modal khi click bên ngoài modal
            var option = { backdrop: 'static' };

            // kiểm tra (logic, điều kiện...)

            // load modal
            $modalDialog.load(href, function () {
                $modal.modal(option, 'show');
            });
        });

    }); // document ready
});