﻿using Core.Models.APIModels;
using Core.Models.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MyServices.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace OrdersManagerment.Auth.Author
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CusAuthorizePermission : AuthorizeAttribute, IAuthorizationFilter
    {
        public string Permissions { get; set; }

        public async void OnAuthorization(AuthorizationFilterContext context)
        {
            if (string.IsNullOrEmpty(Permissions))
            {
                context.Result = new JsonResult(new BaseResponse()
                {
                    StatusCode = StatusCodes.Status401Unauthorized,
                    Message = "Unauthorized"
                });
                //new UnauthorizedResult();
                return;
            }

            var user = (Users)context.HttpContext.Items["User"];
            if (user == null)
            {
                // not logged in
                context.Result = new JsonResult(new BaseResponse()
                {
                    StatusCode = StatusCodes.Status401Unauthorized,
                    Message = "Unauthorized"
                });
                return;
            }
            //var userName = context.HttpContext.User.Identity.Name;
            var someService = context.HttpContext.RequestServices.GetService<RoleServices>();

            var roles = (await someService.GetRoleByUser(user.IDUser)).Select(x => x.Name).ToList();
            //var assignedPermissionsForUser = "ADMIN, CUSTOMER";
            //var assignedPermissionsForUser = "ADMIN, CUSTOMER";
            //MockData.UserPermissions
            //    .Where(x => x.Key == userName)
            //    .Select(x => x.Value).ToList();

            var requiredPermissions = Permissions.Split(",");
            foreach (var x in requiredPermissions)
            {
                if (roles.Contains(x))
                    return;
            }

            context.Result = new UnauthorizedResult();
            return;
        }
    }
}
