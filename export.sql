-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: nhmiaxqk_nendb
-- ------------------------------------------------------
-- Server version	5.5.5-10.6.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banner` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) CHARACTER SET utf8mb3 DEFAULT NULL,
  `Link` varchar(500) CHARACTER SET utf8mb3 NOT NULL,
  `Order` int(11) DEFAULT 1,
  `IsAppear` int(11) DEFAULT 1,
  `ImgName` varchar(100) CHARACTER SET utf8mb3 NOT NULL,
  `ImgPath` varchar(500) CHARACTER SET utf8mb3 NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bill` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDUser` int(11) NOT NULL,
  `IDCart` int(11) NOT NULL,
  `TotalPrice` float NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `Updated` datetime DEFAULT NULL,
  `DeliveryDate` datetime DEFAULT NULL,
  `RecieveDate` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `IsDelete` int(11) DEFAULT 0,
  `IDPerson` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (1,1,1,10000,'2020-11-01 00:00:00',0,NULL,NULL,NULL,NULL,1,0,1);
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) CHARACTER SET utf8mb3 NOT NULL,
  `Content` varchar(5000) CHARACTER SET utf8mb3 NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `IsAppear` int(11) DEFAULT 1,
  `ImgName` varchar(200) CHARACTER SET utf8mb3 DEFAULT NULL,
  `ImgPath` varchar(500) CHARACTER SET utf8mb3 DEFAULT NULL,
  `IDCategory` int(11) DEFAULT 0,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDPerson` int(11) NOT NULL,
  `IDProducts` varchar(500) CHARACTER SET utf8mb3 DEFAULT NULL,
  `TotalPrice` float NOT NULL DEFAULT 0,
  `Created` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `IsDelete` int(11) DEFAULT 0,
  `ArrAmount` varchar(500) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) CHARACTER SET utf8mb3 NOT NULL,
  `Description` varchar(2000) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `Slug` varchar(500) CHARACTER SET utf8mb3 DEFAULT NULL,
  `Material` varchar(50) CHARACTER SET utf8mb3 DEFAULT NULL,
  `IDParent` int(11) DEFAULT 0,
  `IsAppear` int(11) DEFAULT 1,
  `ImgName` varchar(500) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `ImgPath` varchar(500) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `Type` varchar(45) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `CreateBy` int(11) NOT NULL DEFAULT 0,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (38,'Nến thơm',NULL,'nen-thom','New Arrivals',0,1,NULL,NULL,'feature',0,NULL,NULL,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) CHARACTER SET utf8mb3 DEFAULT NULL,
  `Link` varchar(500) CHARACTER SET utf8mb3 NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `StartTime` datetime NOT NULL,
  `EndTime` datetime NOT NULL,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `IsAppear` int(11) DEFAULT 1,
  `ImgName` varchar(100) CHARACTER SET utf8mb3 NOT NULL,
  `ImgPath` varchar(500) CHARACTER SET utf8mb3 NOT NULL,
  `Order` int(11) DEFAULT 0,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'nến thơm 4','http://lim.vn','2020-09-17 21:42:31',1,'2020-09-17 12:45:00','2020-09-18 13:46:00','0001-01-01 00:00:00',0,1,'1.jpg','~/images/1.jpg',1);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imageproduct`
--

DROP TABLE IF EXISTS `imageproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `imageproduct` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) CHARACTER SET utf8mb3 NOT NULL,
  `Path` varchar(1000) CHARACTER SET utf8mb3 NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `IsAppear` int(11) DEFAULT 1,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imageproduct`
--

LOCK TABLES `imageproduct` WRITE;
/*!40000 ALTER TABLE `imageproduct` DISABLE KEYS */;
INSERT INTO `imageproduct` VALUES (51,'1.jpg','~/images/products/1.jpg','2020-09-10 21:03:17',1,NULL,0,1),(52,'99e0857b010c919f1f3eba0b6cec096a.jpg','~/images/products/99e0857b010c919f1f3eba0b6cec096a.jpg','2021-07-12 00:36:44',1,NULL,0,1),(53,'185143853_3951497101632907_7894463193293516183_n.jpg','~/images/products/185143853_3951497101632907_7894463193293516183_n.jpg','2021-07-22 22:21:04',1,NULL,0,1),(54,'185143853_3951497101632907_7894463193293516183_n.jpg','~/images/products/185143853_3951497101632907_7894463193293516183_n.jpg','2021-07-22 22:25:18',1,NULL,0,1);
/*!40000 ALTER TABLE `imageproduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) CHARACTER SET utf8mb3 NOT NULL,
  `Material` varchar(200) CHARACTER SET utf8mb3 NOT NULL,
  `IsAppear` int(11) DEFAULT 1,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (15,'Khuyến mại','Sales',1),(16,'Hàng mới về','New Arrivals',1);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `person` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FullName` varchar(200) CHARACTER SET utf8mb3 NOT NULL,
  `Address` varchar(1000) CHARACTER SET utf8mb3 NOT NULL,
  `Email` varchar(200) CHARACTER SET utf8mb3 NOT NULL,
  `PhoneNumber` varchar(20) CHARACTER SET utf8mb3 NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `IsDelete` int(11) DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:20:20',1,NULL,0,0),(2,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:20:49',1,NULL,0,0),(3,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:21:56',1,NULL,0,0),(4,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:24:27',1,NULL,0,0),(5,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:25:01',1,NULL,0,0),(6,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:25:14',1,NULL,0,0),(7,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:25:24',1,NULL,0,0),(8,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:34:28',1,NULL,0,0),(9,'linh','hn','adsg@gmail.com','0912384678','2020-09-05 16:35:04',1,NULL,0,0);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(500) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(2000) DEFAULT NULL,
  `Detail` varchar(100) DEFAULT NULL,
  `Producer` varchar(200) DEFAULT NULL,
  `Quantity` int(11) DEFAULT 0,
  `Cost` float NOT NULL DEFAULT 0,
  `Category` varchar(200) DEFAULT NULL,
  `Price` float NOT NULL DEFAULT 0,
  `Interest` float NOT NULL DEFAULT 0,
  `Discount` float DEFAULT NULL,
  `MarketPrice` float DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `ManufacetureYear` int(11) DEFAULT NULL,
  `Material` varchar(100) DEFAULT NULL,
  `Ship` double DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `Size` varchar(100) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `IDImages` varchar(200) DEFAULT NULL,
  `Created` datetime NOT NULL,
  `Expired` datetime DEFAULT NULL,
  `CreatedBy` int(11) NOT NULL,
  `Updated` datetime DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `IsDelete` int(11) DEFAULT NULL,
  `RateStar` int(11) DEFAULT NULL,
  `Buys` int(11) DEFAULT 0,
  `Slug` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (16,'Nến (XS)','NEN','a','a',NULL,10,100000,NULL,250000,0,0,500000,NULL,0,NULL,0,NULL,NULL,1,'52','2021-07-12 00:36:44',NULL,1,NULL,0,0,5,0,'nen-xs'),(17,'ddg (S)','eewe','ág','ddg',NULL,10,100000,'38',200000,0,0,500000,NULL,0,NULL,10000,NULL,NULL,1,'53','2021-07-22 22:21:04',NULL,1,NULL,0,0,5,0,'ddg-s'),(18,'sdfs (S)','ee',NULL,NULL,'VN',10,10000,'38',20020,0,0,502052,NULL,0,NULL,0,NULL,NULL,1,'54','2021-07-22 22:25:18',NULL,1,NULL,0,0,5,0,'sdfs-s'),(19,'Nến (L)','NENL','đẹp',NULL,NULL,10,250000,NULL,250000,250000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-09-01 10:22:47',NULL,1,NULL,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `IDRole` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) CHARACTER SET utf8mb3 NOT NULL,
  `Author` varchar(200) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`IDRole`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'DashBoard','Quản lý dashboard'),(2,'CATEGORY','Quản lý danh mục'),(3,'PRODUCTS','Quản lý sản phẩm'),(4,'ADMIN','Quản lý hết');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userrole` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDUser` int(11) NOT NULL,
  `IDRole` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrole`
--

LOCK TABLES `userrole` WRITE;
/*!40000 ALTER TABLE `userrole` DISABLE KEYS */;
INSERT INTO `userrole` VALUES (1,1,4);
/*!40000 ALTER TABLE `userrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `IDUser` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Id` varchar(100) COLLATE utf8mb3_unicode_ci NOT NULL,
  `Username` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL,
  `Password` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8mb3 NOT NULL,
  `LatestLogin` datetime DEFAULT NULL,
  `Email` varchar(45) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDUser`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'da2dee4a-a612-4c8e-b310-372790017a54','admin','c746b67ced8edddd108dc404c22f9962','Linh',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'nhmiaxqk_nendb'
--

--
-- Dumping routines for database 'nhmiaxqk_nendb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-01 17:18:14
