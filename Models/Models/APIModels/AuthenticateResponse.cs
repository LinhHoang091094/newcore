﻿using Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.APIModels
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }


        public AuthenticateResponse(Users user, string token)
        {
            Id = user.IDUser;
            UserName = user.UserName;
            Token = token; 
        }
    }
}
