﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.APIModels
{
    public class BaseResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }
}
