﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Entity
{
    public class Product
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public float Interest { get; set; }
        public float Cost { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
    }
}
