﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Entity
{
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
