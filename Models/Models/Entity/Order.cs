﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Entity
{
    public class Order
    {
        public int ID { get; set; }
        public int IDCustomer { get; set; }
        public int IDProduct { get; set; }
        public int Amount { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
