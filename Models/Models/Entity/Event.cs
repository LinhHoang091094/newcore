﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Entity
{
    public class Event
    {
        public int ID { get; set; }
        public int Order { get; set; }
        public int IsAppear { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string ImgName { get; set; }
        public string ImgPath { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
    }
}
