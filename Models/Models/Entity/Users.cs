﻿using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Models.Entity
{
    public class Users : IdentityUser
    {
        public int IDUser { get; set; }
        //public string UserName { get; set; }
        //[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{6,20}$",
        //ErrorMessage = "Password must meet requirements")]
        public string Password { get; set; }
        //--------------------------------
        [StringLength(255)]
        public string Name { get; set; }
        [NotMapped]
        public DateTime? LatestLogin { get; set; }
    }
}
