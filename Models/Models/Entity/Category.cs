﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Entity
{
    public class Category
    {
        public int ID { get; set; }
        public int IDParent { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
    }
}
