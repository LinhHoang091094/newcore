﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Entity
{
    public class UserRole
    {
        public int ID { get; set; }
        public int IDUser { get; set; }
        public int IDRole { get; set; }
    }
}
