﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ViewModels
{
    public class BaseViewModel
    {
        public string Action { get; set; }
        public string URL { get; set; }
        public int PageCount { get; set; }
        public int CurrentPageIndex { get; set; }
        public int TypeAction { get; set; }
    }
}
