﻿using Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ViewModels
{
    public class CategoryViewModel:BaseViewModel
    {
        public List<Category> Categories { get; set; }
        public List<Product> Products { get; set; }
    }
}
