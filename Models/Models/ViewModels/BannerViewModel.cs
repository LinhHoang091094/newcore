﻿using Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ViewModels
{
    public class BannerViewModel:BaseViewModel
    {
        public List<Banner> Banners { get; set; }
    }
}
