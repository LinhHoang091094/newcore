﻿using Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public Customer Customer { get; set; }
    }
}
