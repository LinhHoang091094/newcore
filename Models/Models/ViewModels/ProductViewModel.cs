﻿using Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ViewModels
{
    public class ProductViewModel:BaseViewModel
    {
        public List<Customer> Customers { get; set; }
        public List<Product> Products { get; set; }
    }
}
