﻿using Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ViewModels
{
    public class EventViewModel:BaseViewModel
    {
        public List<Event> Events { get; set; }
    }
}
